# @ipui/ipfs

reactjs ipfs context

> This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## ip[fn]s links

## description

this provider uses the ipfs-http-client to bind an entire ipfs api accesible from an react component.

## technologies

* [reactjs](https://reactjs.org)
* [ipfs](https://ipfs.io)
* [ipfs-http-client](https://github.com/ipfs/js-ipfs-http-client)

## depends on

* [@ipui/path](https://gitlab.com/ipui/ipui-path)

## how it works

* it uses the [context api](https://reactjs.org/docs/context.html) of reactjs to provide an accessible service to any component inside the context itself.

* it uses [ipfs-http-client](https://github.com/ipfs/js-ipfs-http-client) to bind a node instance.

* binds `withIpfs` on the `props` or `this.props` when is used as _consumer_.

## propTypes

| name | default | description |
| - | - | - |
| `noIpfs` | | react element that is rendered when the local `api` is unreachable |

## api

this api variables are accesible by the component that uses `withIpfs` consumer.

### address

contains the multiaddress (`/ip4/127.0.0.1/tcp/5001` by default) of the api.

### node

instance of IpfsHttpClient

### provider

```javascript
import Ipfs from '@ipui/ipfs'

const MyComponent = props => {

  return (
    <Ipfs noIpfs={ ( <h1>no local ipfs node available</h1> ) }>
      
      { /*
        your app code that requires a ipfs provider
      */}

    </Ipfs>
  )
}

export default MyComponent
```

### consumer

get node identity

```javascript
import { withIpfs } from '@ipui/ipfs'

const NodeId = props => {

  function handleClick() {
    const { id } = props.withIpfs.node
    id( ( err, identity ) => {
      if ( !err )
        console.log( identity )
    } )
  }

  return (
    <button onClick={ handleClick }>
      get node identity
    </button>
  )
}

export default withIpfs( NodeId )
```

## last updated
aug 26, 2019
