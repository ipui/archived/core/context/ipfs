import React, { Component, createContext } from 'react'
import PropTypes from 'prop-types'
import IpfsHttpClient from 'ipfs-http-client'

import RouteTools from '@ipui/path/dist/lib/RouteTools'

const IpfsContext = createContext()

class Ipfs extends Component {

  state = {
    address: null,
    node: null
  }

  static propTypes = {
    noIpfs: PropTypes.elementType
  }

  constructor( props ) {
    super( props )
    this.connect = this.connect.bind( this )
    this.bootstrap = this.bootstrap.bind( this )
  }

  static getDerivedStateFromError( error ) {
    return { hasError: true }
  }

  componentDidMount() {
    this.bootstrap()
  }

  bootstrap() {
    const parsed = RouteTools.parse()

    if ( !parsed )
      RouteTools.redirect( '/' )

    this.connect( parsed.addresses.api )
  }

  connect( address ) {
    const newNode = IpfsHttpClient( address )
    newNode.id()
      .then( identity => {
        this.setState( { ...this.state, address: address, node: newNode } )
      } )
      .catch( err => {
        console.error( 'my error', err )
      } )
  }

  render() {

    const { node, address } = this.state
    if ( !node ) {
      const { noIpfs } = this.props
      if ( noIpfs )
        return React.createElement( noIpfs )

      return ( <>an <a href="https://ipfs.io">ipfs</a> node is required to use this dapp</> )
    }

    return (
      <IpfsContext.Provider value={ {
        address: address,
        node: node
      } }>
        { this.props.children }
      </IpfsContext.Provider>
    )
  }

}

const withIpfs = ( ComponentAlias ) => {

  return props => (
    <IpfsContext.Consumer>
      { context => {
        return <ComponentAlias { ...props } withIpfs={ context } />
      } }
    </IpfsContext.Consumer>
  )

}

export default Ipfs

export {
  IpfsContext,
  withIpfs
}
